package easy.banking;

import java.util.concurrent.Callable;

public class Client {
    Client(String uniqueId, String name) {
        _uniqueId = uniqueId;
        _name = name;
    }

    private String _uniqueId;
    private String _name;

    public String getUniqueId() {
        return _uniqueId;
    }

    public String getName() {
        return _name;
    }

    public void setName(String newName) {
        _name = newName;
    }

    @Override
    public String toString() {
        return "Client{" +
                "\n_uniqueId='" + _uniqueId + '\'' +
                ",\n _name='" + _name + '\'' +
                "}\n";
    }
}
