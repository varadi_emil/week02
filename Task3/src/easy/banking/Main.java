package easy.banking;

import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        Client client1 = new Client("123", "Emil");
        Client client2 = new Client("124", "Amil");
        Client client3 = new Client("125", "Omil");
        Client client4 = new Client("126", "Imil");

        HashMap<Currency, Double> rates = new HashMap<>();
        rates.put(Currency.RON, 1.00);
        rates.put(Currency.EUR, 4.88);
        rates.put(Currency.USD, 4.09);

        Account account1 = new Account(String.format("%s_%s",client1.getUniqueId(),Currency.EUR.label),Currency.EUR, client1);
        Account account2 = new Account(String.format("%s_%s",client1.getUniqueId(),Currency.EUR.label),Currency.EUR, client2);
        Account account3 = new Account(String.format("%s_%s",client1.getUniqueId(),Currency.RON.label),Currency.RON, client3);
        Account account4 = new Account(String.format("%s_%s",client1.getUniqueId(),Currency.USD.label),Currency.USD, client4);

        ArrayList<Client> clients = new ArrayList<>();
        clients.add(client1);
        clients.add(client2);
        clients.add(client3);
        clients.add(client4);

        ArrayList<Account> accounts = new ArrayList<>();
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        accounts.add(account4);

        Bank bank = new Bank("BCR", clients, accounts, rates);

        System.out.println("Inital state");
        System.out.println(bank);

        account1.deposit(5000);
        account2.deposit(6000);
        account3.deposit(7000);
        account4.deposit(8000);
        System.out.println("Deposited a bunch of money");
        System.out.println(bank);

        account1.withdraw(123);
        account2.withdraw(223);
        account3.withdraw(323);
        account4.withdraw(423);
        System.out.println("Withdrew a bit of money");
        System.out.println(bank);
    }
}
