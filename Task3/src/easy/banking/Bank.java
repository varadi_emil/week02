package easy.banking;

import java.util.ArrayList;
import java.util.HashMap;

public class Bank {
    Bank(String name, ArrayList<Client> clients, ArrayList<Account> accounts, HashMap<Currency, Double> rates) {
        _name = name;
        _clients = clients;
        _accounts = accounts;
        _rates = rates;
    }

    private String _name;
    private ArrayList<Client> _clients;
    private ArrayList<Account> _accounts;
    private HashMap<Currency, Double> _rates;

    public void createClient(String uniqueId, String name) {
        Client client = new Client(uniqueId, name);

        _clients.add(client);
    }

    public void createAccount(Client client, Currency currency) {
        String uniqueId = String.format("%s_%s", client.getUniqueId(), currency.label);
        Account account = new Account(uniqueId, currency, client);

        _accounts.add(account);
    }

    @Override
    public String toString() {
        return "Bank{" +
                "\n_name='" + _name + '\'' +
                ",\n _clients=" + _clients.toString() +
                ",\n _accounts=" + _accounts.toString() +
                ",\n _rates=" + _rates.toString() +
                '}';
    }
}
