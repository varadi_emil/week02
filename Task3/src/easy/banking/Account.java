package easy.banking;

public class Account {
    Account(String uniqueId, Currency currency, Client client) {
        _uniqueId = uniqueId;
        _currency = currency;
        _client = client;
        _balance = 0;
    }

    final private String _uniqueId;
    final private Currency _currency;
    final private Client _client;
    private double _balance;

    public String getUniqueId () {
        return _uniqueId;
    }

    public double getBalance() {
        return _balance;
    }

    public void deposit(double amount) {
        _balance += amount;
    }

    public void withdraw(double amount) {
        _balance -= amount;
    }

    @Override
    public String toString() {
        return "Account{" +
                "\n_uniqueId='" + _uniqueId + '\'' +
                ",\n _currency=" + _currency +
                ",\n _client=" + _client +
                ",\n _balance=" + _balance +
                "}\n";
    }
}
