package dice;

public class PairOfDice {
    public PairOfDice() {
        this.firstDie = new Die();
        this.secondDie = new Die();
    }

    private final Die firstDie;
    private final Die secondDie;

    public int getFirstDieFaceValue() {
        return firstDie.getFaceValue();
    }

    public int getSecondDieFaceValue() {
        return secondDie.getFaceValue();
    }

    public void setFirstDieFaceValue(int value) {
        firstDie.setFaceValue(value);
    }

    public void setSecondDieFaceValue(int value) {
        secondDie.setFaceValue(value);
    }

    public void roll() {
        firstDie.roll();
        secondDie.roll();
    }

    public int getSum() {
        return firstDie.getFaceValue() + secondDie.getFaceValue();
    }

    @Override
    public String toString() {
        return "Rolls: " + firstDie.toString() + ", " + secondDie.toString() + "; Sum: " + getSum();
    }
}
