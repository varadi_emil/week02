package shapes.shapes;

import java.lang.Math;

import org.junit.jupiter.api.Test;
import shapes.exceptions.InvalidArgumentException;

import static org.junit.jupiter.api.Assertions.*;

class TriangleTest {
    @Test
    void testArea() {
        double ta = 3;
        double tb = 6;
        double tc = 8;

        double tPerimeter = ta+tb+tc;
        double s= tPerimeter/2.0;
        double tArea = Math.sqrt(s*(s-ta)*(s-tb)*(s-tc));

        Triangle triangle = new Triangle(ta,tb,tc);
        assertEquals(triangle.area(), tArea);
    }

    @Test
    void testPerimeter() {
        double ta = 9;
        double tb = 8;
        double tc = 5;

        double tPerimeter = ta+tb+tc;

        Triangle triangle = new Triangle(ta,tb,tc);
        assertEquals(triangle.perimeter(), tPerimeter);
    }

    @Test
    void testForIncorrectInputs() {
        double ta = -1;
        double tb = 0;
        double tc = 5;

        assertThrows(InvalidArgumentException.class, () -> {
            Triangle triangle = new Triangle(ta, tb, tc);
        });
    }

    @Test
    void testForImpossibleSideLengths() {
        double ta = 10;
        double tb = 2;
        double tc = 3;

        assertThrows(InvalidArgumentException.class, () -> {
            Triangle triangle = new Triangle(ta, tb, tc);
        });
    }
}