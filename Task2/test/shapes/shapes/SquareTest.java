package shapes.shapes;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import shapes.exceptions.InvalidArgumentException;

import static org.junit.jupiter.api.Assertions.*;

class SquareTest {
    @ParameterizedTest
    @ValueSource (doubles = {1,2,3,4,5})
    void testArea(double sideLength) {
        Square square = new Square(sideLength);

        assertEquals(sideLength*sideLength, square.area());
    }

    @ParameterizedTest
    @ValueSource (doubles = {1,2,3,4,5})
    void testPerimeter(double sideLength) {
        Square square = new Square(sideLength);

        assertEquals(sideLength*4.0, square.perimeter());
    }

    @ParameterizedTest
    @ValueSource (doubles = {-2.0, -1.0, 0.0})
    void testInvalidArguments(double sideLength) {
        assertThrows(InvalidArgumentException.class, () -> {
            Square square = new Square(sideLength);
        });
    }
}