package shapes.shapes;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import shapes.exceptions.InvalidArgumentException;

class RectangleTest {
    @Test
    void testArea() {
        double ta = 4;
        double tb = 5;

        double tArea = ta*tb;

        Rectangle rectangle = new Rectangle(ta, tb);
        assertEquals(tArea, rectangle.area());
    }

    @Test
    void testPerimeter() {
        double ta = 4;
        double tb = 5;

        double tPerimeter = 2.0*(ta + tb);

        Rectangle rectangle = new Rectangle(ta, tb);
        assertEquals(tPerimeter, rectangle.perimeter());
    }

    @Test
    void testInvalidArgs() {
        double ta = 0.0;
        double tb = -1.2;

        assertThrows(InvalidArgumentException.class, () -> {
            Rectangle rectangle = new Rectangle(ta, tb);
        });
    }
}