package shapes.shapes;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import shapes.exceptions.InvalidArgumentException;

import static org.junit.jupiter.api.Assertions.*;

class CircleTest {
    @Test
    void testArea() {
        double tRadius = 4;
        double tArea = Math.PI * tRadius*tRadius;

        Circle circle = new Circle(tRadius);
        assertEquals(tArea, circle.area());
    }

    @Test
    void testPerimeter() {
        double tRadius = 4;
        double tPerimeter = 2.0*Math.PI*tRadius;

        Circle circle = new Circle(tRadius);
        assertEquals(tPerimeter, circle.perimeter());
    }

    @ParameterizedTest
    @ValueSource(doubles = {-1.0, 0.0, -2.3})
    void testInvalidInput(double radius) {
        assertThrows(InvalidArgumentException.class, () -> {
            Circle circle = new Circle(radius);
        });
    }
}