package shapes.contracts;

import shapes.exceptions.InvalidArgumentException;

public interface Shape {
    public double area();
    public double perimeter();

    @Override
    public String toString();
}
