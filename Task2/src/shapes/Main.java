package shapes;

import shapes.contracts.Shape;
import shapes.exceptions.InvalidArgumentException;
import shapes.shapes.*;

public class Main {

    public static void main(String[] args) {
        ShapeBuilder rectangleBuilder = () -> new Rectangle(-2,3);
        shapeDemo(rectangleBuilder);

        ShapeBuilder circleBuilder = () -> new Circle(4);
        shapeDemo(circleBuilder);

        ShapeBuilder squareBuilder = () -> new Square(7);
        shapeDemo(squareBuilder);

        ShapeBuilder triangleBuilder = () -> new Triangle(2,3,4);
        shapeDemo(triangleBuilder);
    }

    static void shapeDemo(ShapeBuilder builder) {
        try {
            Shape shape = builder.build();

            System.out.println(shape.toString());
            String message = String.format("Area: %.2f; Perimeter: %.2f;", shape.area(), shape.perimeter());
            System.out.println(message);
        }
        catch (InvalidArgumentException ex) {
           System.out.println("Invalid args! Initiate Self Destruct Sequence!");
        }
        catch (Exception ex) {
            System.out.println("Unexpected Error!");
        }
    }
}

@FunctionalInterface
interface ShapeBuilder {
    Shape build();
}
