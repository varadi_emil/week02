package shapes.shapes;

import shapes.contracts.Shape;
import shapes.exceptions.InvalidArgumentException;

import java.util.ArrayList;

public class Rectangle implements Shape {
    public Rectangle(double a, double b) throws InvalidArgumentException {
        ArrayList<Double> args = new ArrayList<>();
        args.add(a);
        args.add(b);

        if(args.stream().anyMatch(e -> e <= 0.0)) {
            throw new InvalidArgumentException();
        }

        _a = a;
        _b = b;
    }

    private final double _a;
    private final double _b;

    @Override
    public double area() {
        return _a*_b;
    }

    @Override
    public double perimeter() {
        return 2.0*(_a + _b);
    }

    @Override
    public String toString() {
        return String.format("This Rectangle has sides of: %.2f and %.2f.", _a, _b);
    }
}
