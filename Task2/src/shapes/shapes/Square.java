package shapes.shapes;

import shapes.contracts.Shape;
import shapes.exceptions.InvalidArgumentException;

public class Square implements Shape {
    public Square(double sideLength) throws InvalidArgumentException {
        if(sideLength <= 0.0) {
            throw new InvalidArgumentException();
        }

        _sideLength = sideLength;
    }

    private final double _sideLength;

    @Override
    public double area() {
        return _sideLength*_sideLength;
    }

    @Override
    public double perimeter() {
        return 4.0*_sideLength;
    }

    @Override
    public String toString() {
        return String.format("This Square has sides of: %.2f.", _sideLength);
    }
}
