package shapes.shapes;

import shapes.contracts.Shape;
import shapes.exceptions.InvalidArgumentException;

public class Circle implements Shape {
    public Circle(double radius) throws InvalidArgumentException {
        if(radius <= 0.0) {
            throw  new InvalidArgumentException();
        }

        _radius = radius;
    }

    private final double _radius;

    @Override
    public double area() {
        return Math.PI*Math.pow(_radius, 2.0);
    }

    @Override
    public double perimeter() {
        return 2.0*Math.PI*_radius;
    }

    @Override
    public String toString() {
        return String.format("This Circle has a radius of: %.2f.", _radius);
    }
}
