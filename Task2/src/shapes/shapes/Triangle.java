package shapes.shapes;

import java.util.*;

import shapes.contracts.Shape;
import shapes.exceptions.InvalidArgumentException;

public class Triangle implements Shape{
    public Triangle(double a, double b, double c) throws InvalidArgumentException {
        ArrayList<Double> args = new ArrayList<>();
        args.add(a);
        args.add(b);
        args.add(c);

        if(args.stream().anyMatch(value -> value <= 0.0)) {
            throw new InvalidArgumentException();
        }

        Collections.sort(args);

        if(args.get(0) + args.get(1) <= args.get(2)) {
            throw new InvalidArgumentException();
        }

        _a = a;
        _b = b;
        _c = c;
    }

    private final double _a;
    private final double _b;
    private final double _c;

    @Override
    public double area() {
        double s = (_a + _b + _c)/2.0;
        return Math.sqrt(s*(s-_a)*(s-_b)*(s-_c));
    }

    @Override
    public double perimeter() {
        return _a+_b+_c;
    }

    @Override
    public String toString() {
        return String.format("This Triangle has sides of: %.2f %.2f and %.2f.", _a, _b, _c);
    }
}
