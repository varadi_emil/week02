package laptop;

public class Laptop {
    public Laptop(String model, String make, String purchaser, int purchaseYear) {
        this.model = model;
        this.make = make;
        this.purchaser = purchaser;
        this.purchaseYear = purchaseYear;
    }

    private String model;
    private String make;
    private String purchaser;
    private int purchaseYear;

    public String getModel() {
        return model;
    }

    public String getMake() {
        return make;
    }

    public String getPurchaser() {
        return purchaser;
    }

    public int getPurchaseYear() {
        return purchaseYear;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public void setPurchaser(String purchaser) {
        this.purchaser = purchaser;
    }

    public void setPurchaseYear(int purchaseYear) {
        this.purchaseYear = purchaseYear;
    }

    @Override
    public String toString() {
        return "\n\nLaptop {\n" +
                "\tmodel: " + model + "\n" +
                "\tmake: " + make + "\n" +
                "\tpurchaser: " + purchaser + "\n" +
                "\tpurchaseYear: " + purchaseYear + "\n" +
                "}\n\n";
    }
}
