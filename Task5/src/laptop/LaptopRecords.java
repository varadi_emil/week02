package laptop;

public class LaptopRecords {
    public static void run() {
        Laptop laptop1 = new Laptop(
                "DELL 123",
                "Plastic",
                "Chad Chadington",
                2019
        );

        Laptop laptop2 = new Laptop(
                "ASUS 666",
                "Stainless steel",
                "The pope",
                1574
        );

        Laptop laptop3 = new Laptop(
                "Lenovo -200",
                "Wood",
                "Mr. Consoomer",
                2020
        );

        Laptop laptop4 = new Laptop(
                "Hewlett Packard 456",
                "Unknown",
                "FBI agent 6702",
                1978
        );

        System.out.println(laptop1);
        System.out.println(laptop2);
        System.out.println(laptop3);
        System.out.println(laptop4);

        laptop1.setModel("IBM 999");
        laptop2.setMake("Wakanda steel");
        laptop3.setPurchaser("Consoomer jr.");
        laptop4.setPurchaseYear(1900);

        System.out.println(laptop1);
        System.out.println(laptop2);
        System.out.println(laptop3);
        System.out.println(laptop4);
    }
}
