# Homework Week 2

## Váradi Zoltán-Emil
##### Computational physics, year 2

### 0. Dependencies
openjdk 11
All tasks were implemented using IntelliJ, opening them in the same IDE is recommended.
Projects were created in IntelliJ, some of them use Gradle
Occasional dependencied: JUnit5, Mockito, Guava 

### 1. Assignment 1 reupload
Version 2.0 of assignment 1 can be found on the teams platform.

### 2. Shapes in java
Implemented a package that can create 2D shapes and determine their area and perimeter. 

Sources: Task2/*

### 3. Simplified banking system
This is a simplified version of a simple banking system. A better version will be available in Task4.
See Task4 for a better implementation.

Sources: Task3/*

### 4. Banking system
This a better version of the simple banking system from the previous chapter.
This was probably overkill.
The project was created in IntelliJ, using Gradle. Java version 11.
Dependencies: JUnit5, Mockito, Guava
logs.txt holds the output for a test run.

Test coverage for most things.
Idea was to use something like the "Clean Architecture". Layers of the project are loosely coupled.
Interfaces allowed for easy testing and mocking other layers.
Achieved dependency inversion. And used dependency injection.

Sources: Task4/*

### 5. Laptop records
Laptop data model, used by LaptopRecords. Run by main, state of the records are printed to the screen.

Sources: Task5/*

### 6. Rolling the die
Rolling 2 dice and printing individual face values and the sum.

Sources: Task6/*
