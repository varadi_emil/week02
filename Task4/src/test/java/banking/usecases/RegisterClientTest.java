package banking.usecases;

import banking.contracts.Bank;
import banking.entities.Client;
import banking.models.OperationResponse;
import banking.usecases.params.CreateAccountParams;
import banking.usecases.params.RegisterClientParams;
import jdk.jfr.Name;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RegisterClientTest {
    @Mock
    Bank mockBank;

    @Test
    @Name("should call register a client")
    void testRegisterClient() {
        RegisterClient useCase = new RegisterClient(mockBank);
        RegisterClientParams params = new RegisterClientParams("Emil");

        Client tClient = new Client("123", "name");
        when(mockBank.registerClient(anyString())).thenReturn(new OperationResponse<Client>(true, tClient));

        OperationResponse<Client> result = useCase.call(params);
        verify(mockBank).registerClient(params.name);
        assertTrue(result.success);
        assertEquals(result.data, tClient);
    }
}