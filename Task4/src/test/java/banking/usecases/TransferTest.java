package banking.usecases;

import banking.contracts.Bank;
import banking.entities.Receipt;
import banking.factories.ReceiptFactory;
import banking.models.OperationResponse;
import banking.usecases.params.TransferParams;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TransferTest {

    @Mock
    Bank mockBank;

    @Test
    void testTransfer() {
        Transfer usecase = new Transfer(mockBank);

        TransferParams params = new TransferParams("123", "123", "123", 1000);
        when(mockBank.transfer(anyString(), anyString(), anyString(), anyDouble()))
                .thenReturn(new OperationResponse<>(true, null));

        OperationResponse<Receipt> response = usecase.call(params);

        verify(mockBank).transfer(params.senderClientId, params.senderAccountId, params.targetAccountId, params.amount);
        assertTrue(response.success);
    }
}