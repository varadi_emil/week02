package banking.usecases;

import banking.contracts.Bank;
import banking.entities.Receipt;
import banking.models.OperationResponse;
import banking.usecases.params.DepositParams;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DepositTest {

    @Mock
    Bank mockBank;

    @Test
    void testDeposit() {
        Deposit usecase = new Deposit(mockBank);

        DepositParams params = new DepositParams("123", "123", 1000);
        double tNewBalance = params.amount;
        when(mockBank.deposit(anyString(), anyString(), anyDouble()))
                .thenReturn(new OperationResponse<>(true, null));

        OperationResponse<Receipt> response = usecase.call(params);

        verify(mockBank).deposit(params.clientId, params.accountId, params.amount);
        assertTrue(response.success);
        assertEquals(tNewBalance, params.amount);
    }
}