package banking.usecases;

import banking.contracts.Bank;
import banking.entities.Receipt;
import banking.models.OperationResponse;
import banking.usecases.params.WithdrawParams;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class WithdrawTest {

    @Mock
    Bank mockBank;

    @Test
    void testWithdraw() {
        Withdraw usecase = new Withdraw(mockBank);

        WithdrawParams params = new WithdrawParams("123", "123", 1000);

        when(mockBank.withdraw(anyString(), anyString(), anyDouble()))
                .thenReturn(new OperationResponse<Receipt>(true, null));

        OperationResponse<Receipt> response = usecase.call(params);
        verify(mockBank).withdraw(params.clientId, params.accountId, params.amount);
        assertTrue(response.success);
    }
}