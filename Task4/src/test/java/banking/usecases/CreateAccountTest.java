package banking.usecases;

import banking.contracts.Bank;
import banking.entities.Account;
import banking.entities.Currency;
import banking.models.OperationResponse;
import banking.usecases.params.CreateAccountParams;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class CreateAccountTest {

    @Mock
    Bank mockBank;

    @Test
    void testCreateAccount() {
        CreateAccount usecase = new CreateAccount(mockBank);

        Account tAccount = new Account("123", "123", Currency.EUR);
        CreateAccountParams params = new CreateAccountParams(tAccount.getOwnerId(), tAccount.getCurrency());

        when(mockBank.createAccount(anyString(), any()))
                .thenReturn(new OperationResponse<>(true, tAccount));

        OperationResponse<Account> result = usecase.call(params);

        verify(mockBank).createAccount(tAccount.getOwnerId(), tAccount.getCurrency());
        assertTrue(result.success);
        assertEquals(result.data, tAccount);
    }
}