package banking.datasources;

import banking.entities.Account;
import banking.entities.Client;
import banking.entities.Currency;
import banking.models.OperationResponse;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TotallyReliableDataSourceTest {

    @Test
    void testRegisterClient() {
        TotallyReliableDataSource dataSource = new TotallyReliableDataSource();

        String tName = "Emil";

        OperationResponse<Client> response = dataSource.registerClient(tName);

        assertTrue(response.success);

        System.out.println(response.data);
    }

    @Test
    void testCreateAccount() {
        TotallyReliableDataSource dataSource = new TotallyReliableDataSource();

        String tName = "Emil";
        Currency tCurrency = Currency.EUR;

        OperationResponse<Client> response = dataSource.registerClient(tName);
        Client client = response.data;
        assertTrue(response.success);
        System.out.println(response.data);

        OperationResponse<Account> accountResponse = dataSource.createAccount(client.getUniqueId(), tCurrency);
        Account account = accountResponse.data;
        assertTrue(accountResponse.success);
        assertEquals(account.getOwnerId(), client.getUniqueId());
        System.out.println(account);
    }

    @Test
    void testUpdateAccount() {
        TotallyReliableDataSource dataSource = new TotallyReliableDataSource();

        String tName = "Emil";
        Currency tCurrency = Currency.EUR;
        double tNewAmount = 1000;

        OperationResponse<Client> response = dataSource.registerClient(tName);
        Client client = response.data;
        assertTrue(response.success);
        System.out.println(response.data);

        OperationResponse<Account> accountResponse = dataSource.createAccount(client.getUniqueId(), tCurrency);
        Account account = accountResponse.data;
        assertTrue(accountResponse.success);
        assertEquals(account.getOwnerId(), client.getUniqueId());
        System.out.println(account);

        dataSource.updateAccount(
            new Account(
                account.getUniqueId(),
                account.getOwnerId(),
                account.getCurrency(),
                tNewAmount
            )
        );

        assertEquals(account.getBalance(), tNewAmount);
    }
}