package banking.bank;

import banking.contracts.DataSource;
import banking.entities.Account;
import banking.entities.Client;
import banking.entities.Currency;
import banking.entities.Receipt;
import banking.errors.InsufficientBalanceError;
import banking.errors.InvalidArgumentsError;
import banking.models.OperationResponse;
import com.google.common.hash.Hashing;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.Random;

@ExtendWith(MockitoExtension.class)
class RaiffeisenBankTest {

    @Mock
    DataSource mockDataSource;

    @Test
    void testCreateClient() {
        String tName = "Varadi Emil";

        RaiffeisenBank bank = new RaiffeisenBank(mockDataSource);
        Random rng = new Random();
        Integer salt = rng.nextInt();

        Client tClient = new Client(
                Hashing.sha256().hashString(tName + salt.toString()).toString(),
                tName
        );


        when(mockDataSource.registerClient(tName))
                .thenReturn(new OperationResponse<>(true, tClient));

        OperationResponse<Client> response = bank.registerClient(tName);

        verify(mockDataSource).registerClient(tClient.getName());
        assertTrue(response.success);
        assertEquals(response.data, tClient);
    }

    @Test
    void testCreateAccount() {
        RaiffeisenBank bank = new RaiffeisenBank(mockDataSource);

        Client tClient = new Client(
                "123",
                "Name"
        );

        Account tAccount = new Account(
                "123",
                "123",
                Currency.EUR
        );

        when(mockDataSource.createAccount(tClient.getUniqueId(), tAccount.getCurrency()))
                .thenReturn(new OperationResponse<>(true, tAccount));

        OperationResponse<Account> response = bank.createAccount(tClient.getUniqueId(), tAccount.getCurrency());

        verify(mockDataSource).createAccount(tClient.getUniqueId(), tAccount.getCurrency());
        assertTrue(response.success);
        assertEquals(response.data, tAccount);
    }

    /// Deposit
    @ParameterizedTest
    @ValueSource(doubles = {-1.0, 0.0, 1200})
    void testDeposit(double amount) {
        RaiffeisenBank bank = new RaiffeisenBank(mockDataSource);

        Client tClient = new Client(
                "123",
                "Name"
        );

        Account tAccount = new Account(
                "123",
                "123",
                Currency.EUR
        );

        if(amount > 0.0) {
            when(mockDataSource.fetchClient(anyString()))
                    .thenReturn(new OperationResponse<>(true, tClient));

            when(mockDataSource.fetchAccount(anyString()))
                    .thenReturn(new OperationResponse<>(true, tAccount));
        }

        OperationResponse<Receipt> response = bank.deposit(tClient.getUniqueId(), tAccount.getUniqueId(), amount);

        if(amount <= 0.0) {
            assertFalse(response.success);
            verify(mockDataSource, never()).fetchClient(tClient.getUniqueId());
            verify(mockDataSource, never()).fetchAccount(tAccount.getUniqueId());
            assertTrue(response.error instanceof InvalidArgumentsError);
        }
        else {
            verify(mockDataSource).fetchClient(tClient.getUniqueId());
            verify(mockDataSource).fetchAccount(tAccount.getUniqueId());

            assertTrue(response.success);
            assertNotNull(response.data);
        }
    }

    @Test
    void testDepositForBadClientAccountPair() {
        RaiffeisenBank bank = new RaiffeisenBank(mockDataSource);

        Client tClient = new Client(
                "123",
                "Name"
        );

        Account tAccount = new Account(
                "123",
                "124",
                Currency.EUR
        );

        when(mockDataSource.fetchClient(anyString()))
                .thenReturn(new OperationResponse<>(true, tClient));

        when(mockDataSource.fetchAccount(anyString()))
                .thenReturn(new OperationResponse<>(true, tAccount));

        OperationResponse<Receipt> response = bank.deposit(tClient.getUniqueId(), tAccount.getUniqueId(), 123.0);

        assertFalse(response.success);
        verify(mockDataSource).fetchClient(tClient.getUniqueId());
        verify(mockDataSource).fetchAccount(tAccount.getUniqueId());
        assertTrue(response.error instanceof InvalidArgumentsError);
    }

    @Test
    void testDepositForNonExistingAccounts() {
        RaiffeisenBank bank = new RaiffeisenBank(mockDataSource);

        Client tClient = new Client(
                "123",
                "Name"
        );

        Account tAccount = new Account(
                "123",
                "124",
                Currency.EUR
        );

        when(mockDataSource.fetchClient(anyString()))
                .thenReturn(new OperationResponse<>(false, null));

        OperationResponse<Receipt> response = bank.deposit(tClient.getUniqueId(), tAccount.getUniqueId(), 123.0);
        assertFalse(response.success);
        assertTrue(response.error instanceof InvalidArgumentsError);

        when(mockDataSource.fetchClient(anyString()))
                .thenReturn(new OperationResponse<>(true, tClient));

        when(mockDataSource.fetchAccount(anyString()))
                .thenReturn(new OperationResponse<>(false, null));

        OperationResponse<Receipt> response2 = bank.deposit(tClient.getUniqueId(), tAccount.getUniqueId(), 123.0);

        assertFalse(response2.success);
        verify(mockDataSource, times(2)).fetchClient(tClient.getUniqueId());
        verify(mockDataSource, times(1)).fetchAccount(tAccount.getUniqueId());
        assertTrue(response2.error instanceof InvalidArgumentsError);
    }

    /// Withdraw
    @ParameterizedTest
    @ValueSource(doubles = {-1.0, 0.0, 1200, 10000})
    void testWithdraw(double amount) {
        RaiffeisenBank bank = new RaiffeisenBank(mockDataSource);

        Client tClient = new Client(
                "123",
                "Name"
        );

        Account tAccount = new Account(
                "123",
                "123",
                Currency.EUR
        );

        double tBalance = 1300;
        tAccount.setBalance(tBalance);

        if(amount > 0.0) {
            when(mockDataSource.fetchClient(anyString()))
                    .thenReturn(new OperationResponse<>(true, tClient));

            when(mockDataSource.fetchAccount(anyString()))
                    .thenReturn(new OperationResponse<>(true, tAccount));
        }

        OperationResponse<Receipt> response = bank.withdraw(tClient.getUniqueId(), tAccount.getUniqueId(), amount);

        if(amount <= 0.0) {
            assertFalse(response.success);
            verify(mockDataSource, never()).fetchClient(tClient.getUniqueId());
            verify(mockDataSource, never()).fetchAccount(tAccount.getUniqueId());
            assertTrue(response.error instanceof InvalidArgumentsError);
        }
        else {
            if(amount > tBalance) {
                verify(mockDataSource).fetchClient(tClient.getUniqueId());
                verify(mockDataSource).fetchAccount(tAccount.getUniqueId());

                assertFalse(response.success);
                assertTrue(response.error instanceof InsufficientBalanceError);
            }
            else {
                verify(mockDataSource).fetchClient(tClient.getUniqueId());
                verify(mockDataSource).fetchAccount(tAccount.getUniqueId());

                assertTrue(response.success);
                assertNotNull(response.data);
            }
        }
    }

    @Test
    void testWithdrawForBadClientAccountPair() {
        RaiffeisenBank bank = new RaiffeisenBank(mockDataSource);

        Client tClient = new Client(
                "123",
                "Name"
        );

        Account tAccount = new Account(
                "123",
                "124",
                Currency.EUR
        );

        double tBalance = 10000;
        tAccount.setBalance(tBalance);

        when(mockDataSource.fetchClient(anyString()))
                .thenReturn(new OperationResponse<>(true, tClient));

        when(mockDataSource.fetchAccount(anyString()))
                .thenReturn(new OperationResponse<>(true, tAccount));

        OperationResponse<Receipt> response = bank.withdraw(tClient.getUniqueId(), tAccount.getUniqueId(), 123.0);

        assertFalse(response.success);
        verify(mockDataSource).fetchClient(tClient.getUniqueId());
        verify(mockDataSource).fetchAccount(tAccount.getUniqueId());
        assertTrue(response.error instanceof InvalidArgumentsError);
    }

    @Test
    void testWithdrawForNonExistingAccounts() {
        RaiffeisenBank bank = new RaiffeisenBank(mockDataSource);

        Client tClient = new Client(
                "123",
                "Name"
        );

        Account tAccount = new Account(
                "123",
                "124",
                Currency.EUR
        );

        when(mockDataSource.fetchClient(anyString()))
                .thenReturn(new OperationResponse<>(false, null));

        OperationResponse<Receipt> response = bank.withdraw(tClient.getUniqueId(), tAccount.getUniqueId(), 123.0);
        assertFalse(response.success);
        assertTrue(response.error instanceof InvalidArgumentsError);

        when(mockDataSource.fetchClient(anyString()))
                .thenReturn(new OperationResponse<>(true, tClient));

        when(mockDataSource.fetchAccount(anyString()))
                .thenReturn(new OperationResponse<>(false, null));

        OperationResponse<Receipt> response2 = bank.deposit(tClient.getUniqueId(), tAccount.getUniqueId(), 123.0);

        assertFalse(response2.success);
        verify(mockDataSource, times(2)).fetchClient(tClient.getUniqueId());
        verify(mockDataSource, times(1)).fetchAccount(tAccount.getUniqueId());
        assertTrue(response2.error instanceof InvalidArgumentsError);
    }

    /// Transfer
    @ParameterizedTest
    @ValueSource(doubles = {-1.0, 0.0, 1200, 10000})
    void testTransfer(double amount) {
        RaiffeisenBank bank = new RaiffeisenBank(mockDataSource);

        Client tClient = new Client(
                "123",
                "Name"
        );

        Account tAccount1 = new Account(
                "123",
                "123",
                Currency.EUR
        );

        Account tAccount2 = new Account(
                "321",
                "321",
                Currency.RON
        );

        double tBalance1 = 1300;
        double tBalance2 = 0;
        tAccount1.setBalance(tBalance1);
        tAccount2.setBalance(tBalance2);

        if(amount > 0.0) {
            when(mockDataSource.fetchClient(tClient.getUniqueId()))
                    .thenReturn(new OperationResponse<>(true, tClient));

            when(mockDataSource.fetchAccount(tAccount1.getUniqueId()))
                    .thenReturn(new OperationResponse<>(true, tAccount1));

            if(amount <= tBalance1) {
                when(mockDataSource.fetchAccount(tAccount2.getUniqueId()))
                        .thenReturn(new OperationResponse<>(true, tAccount2));
            }
        }

        OperationResponse<Receipt> response = bank.transfer(tClient.getUniqueId(), tAccount1.getUniqueId(), tAccount2.getUniqueId(), amount);

        if(amount <= 0.0) {
            assertFalse(response.success);
            verify(mockDataSource, never()).fetchClient(tClient.getUniqueId());
            verify(mockDataSource, never()).fetchAccount(tAccount1.getUniqueId());
            verify(mockDataSource, never()).fetchAccount(tAccount2.getUniqueId());
            assertTrue(response.error instanceof InvalidArgumentsError);
        }
        else {
            if(amount > tBalance1) {
                verify(mockDataSource).fetchClient(tClient.getUniqueId());
                verify(mockDataSource).fetchAccount(tAccount1.getUniqueId());

                assertFalse(response.success);
                assertTrue(response.error instanceof InsufficientBalanceError);
            }
            else {
                verify(mockDataSource).fetchClient(tClient.getUniqueId());
                verify(mockDataSource).fetchAccount(tAccount1.getUniqueId());
                verify(mockDataSource).fetchAccount(tAccount2.getUniqueId());

                assertTrue(response.success);
            }
        }
    }

    @Test
    void testTransferForBadClientAccountPair() {
        RaiffeisenBank bank = new RaiffeisenBank(mockDataSource);

        Client tClient = new Client(
                "123",
                "Name"
        );

        Account tAccount = new Account(
                "123",
                "124",
                Currency.EUR
        );

        double tBalance = 10000;
        tAccount.setBalance(tBalance);

        when(mockDataSource.fetchClient(anyString()))
                .thenReturn(new OperationResponse<>(true, tClient));

        when(mockDataSource.fetchAccount(anyString()))
                .thenReturn(new OperationResponse<>(true, tAccount));

        OperationResponse<Receipt> response = bank.transfer(tClient.getUniqueId(), tAccount.getUniqueId(), "123", 123.0);

        assertFalse(response.success);
        verify(mockDataSource).fetchClient(tClient.getUniqueId());
        verify(mockDataSource).fetchAccount(tAccount.getUniqueId());
        assertTrue(response.error instanceof InvalidArgumentsError);
    }

    @Test
    void testTransferForNonExistingAccounts() {
        RaiffeisenBank bank = new RaiffeisenBank(mockDataSource);

        Client tClient = new Client(
                "123",
                "Name"
        );

        Account tAccount1 = new Account(
                "123",
                "123",
                Currency.EUR
        );

        tAccount1.setBalance(100000);

        Account tAccount2 = new Account(
                "321",
                "421",
                Currency.EUR
        );

        when(mockDataSource.fetchClient(anyString()))
                .thenReturn(new OperationResponse<>(false, null));

        OperationResponse<Receipt> response = bank.transfer(tClient.getUniqueId(), tAccount1.getUniqueId(), tAccount2.getUniqueId(), 123.0);
        assertFalse(response.success);
        assertTrue(response.error instanceof InvalidArgumentsError);

        when(mockDataSource.fetchClient(anyString()))
                .thenReturn(new OperationResponse<>(true, tClient));

        when(mockDataSource.fetchAccount(anyString()))
                .thenReturn(new OperationResponse<>(false, null));

        OperationResponse<Receipt> response2 = bank.transfer(tClient.getUniqueId(), tAccount1.getUniqueId(), tAccount2.getUniqueId(), 123.0);

        assertFalse(response2.success);
        assertTrue(response2.error instanceof InvalidArgumentsError);

        when(mockDataSource.fetchClient(anyString()))
                .thenReturn(new OperationResponse<>(true, tClient));

        when(mockDataSource.fetchAccount(tAccount1.getUniqueId()))
                .thenReturn(new OperationResponse<>(true, tAccount1));

        OperationResponse<Receipt> response3 = bank.transfer(tClient.getUniqueId(), tAccount1.getUniqueId(), tAccount2.getUniqueId(), 123.0);

        assertFalse(response3.success);
        assertTrue(response3.error instanceof InvalidArgumentsError);

        verify(mockDataSource, times(3)).fetchClient(tClient.getUniqueId());
        verify(mockDataSource, times(2)).fetchAccount(tAccount1.getUniqueId());
        verify(mockDataSource, times(1)).fetchAccount(tAccount2.getUniqueId());
    }
}