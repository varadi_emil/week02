package banking.bank;

import banking.entities.Currency;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RaiffeisenExchangeTest {

    @Test
    void testConversion() {
        RaiffeisenExchange exchange = new RaiffeisenExchange();

        double tAmount = 1000;
        Currency tCurrency1 = Currency.EUR;
        Currency tCurrency2 = Currency.RON;

        double tCorrectValue = 1000*exchange.getRates().get(tCurrency1)/exchange.getRates().get(tCurrency2);

        assertEquals(tCorrectValue, exchange.convertToCurrency(tAmount, tCurrency1, tCurrency2));
    }


}