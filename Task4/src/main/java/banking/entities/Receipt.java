package banking.entities;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Receipt {
    public Receipt(String message) {
        this.message = message;

        Date now = new Date();
        this.dateTime = dateFormat.format(now);
    }

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    final private String message;
    final private String dateTime;

    @Override
    public String toString() {
        return "\n\n\n------------------------\n" +
                "\tRECEIPT\n" +
                "------------------------\n" +
                dateTime + "\n" +
                "------------------------\n" +
                message + "\n" +
                "------------------------\n\n\n\n";
    }
}
