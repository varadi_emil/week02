package banking.entities;

public enum Currency {
    RON("RON"),
    EUR("EUR"),
    USD("USD");

    public final String label;

    private Currency(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return label;
    }
}
