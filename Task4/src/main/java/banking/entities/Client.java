package banking.entities;

import java.util.Objects;

public class Client {
    public Client(String uniqueId, String name) {
        this.uniqueId = uniqueId;
        this.name = name;
    }

    private final String uniqueId;
    private final String name;

    public String getUniqueId() {
        return uniqueId;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "\nClient {\n" +
                "\tuniqueId: " + uniqueId + "\n" +
                "\tname: " + name + "\n" +
                "}\n";
    }

    public boolean equals(Object other) {
        if(this == other) {
            return true;
        }

        if(other == null) {
            return false;
        }

        if(other instanceof Client) {
            return false;
        }

        Client otherClient = (Client) other;
        return uniqueId.equals(otherClient.getUniqueId()) &&
                name.equals(otherClient.getName());

    }
}
