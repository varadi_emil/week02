package banking.entities;

public class Account {
    public Account(String uniqueId, String ownerId, Currency currency) {
        this.uniqueId = uniqueId;
        this.ownerId = ownerId;
        this.currency = currency;
        balance = 0;
    }

    public Account(String uniqueId, String ownerId, Currency currency, double balance) {
        this.uniqueId = uniqueId;
        this.ownerId = ownerId;
        this.currency = currency;
        this.balance = balance;
    }

    private final String uniqueId;
    private final String ownerId;
    private final Currency currency;
    private double balance;

    public String getUniqueId() {
        return uniqueId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public Currency getCurrency() {
        return currency;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "\nAccount {\n" +
                "\tuniqueId: " + uniqueId + "\n" +
                "\townerId: " + ownerId + "\n" +
                "\tcurrency: " + currency + "\n" +
                "\tbalance: " + balance + "\n" +
                "}\n";
    }

    public boolean equals(Object other) {
        if(this == other) {
            return true;
        }

        if(other == null) {
            return false;
        }

        if(!(other instanceof Account)) {
            return false;
        }

        Account otherAccount = (Account) other;

        return uniqueId.equals(otherAccount.getUniqueId()) &&
                ownerId.equals(otherAccount.getOwnerId()) &&
                currency.equals(otherAccount.getCurrency()) &&
                balance == otherAccount.getBalance();
    }
}
