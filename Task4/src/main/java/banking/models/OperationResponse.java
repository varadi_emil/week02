package banking.models;

import banking.errors.NoError;

public class OperationResponse<Type> {
    public OperationResponse(boolean success, Type data) {
       this.success = success;
       this.data = data;
       this.error = new NoError();
    }

    public OperationResponse(Exception error) {
        this.success = false;
        this.data = null;
        this.error = error;
    }

    public final boolean success;
    public final Type data;
    public final Exception error;
}
