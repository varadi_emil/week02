package banking.bank;

import banking.contracts.Exchange;
import banking.entities.Currency;

import java.util.HashMap;

public class RaiffeisenExchange implements Exchange {
    public RaiffeisenExchange() {
        rates = new HashMap<Currency, Double>();

        rates.put(Currency.RON, 1.0);
        rates.put(Currency.EUR, 4.88);
        rates.put(Currency.USD, 4.09);
    }

    final private HashMap<Currency, Double> rates;

    @Override
    public HashMap<Currency, Double> getRates() {
        return rates;
    }

    @Override
    public double convertToCurrency(double amount, Currency c1, Currency c2) {
        return amount*rates.get(c1)/rates.get(c2);
    }
}
