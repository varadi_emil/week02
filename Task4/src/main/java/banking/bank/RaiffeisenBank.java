package banking.bank;

import banking.contracts.Bank;
import banking.contracts.DataSource;
import banking.contracts.Exchange;
import banking.entities.Account;
import banking.entities.Client;
import banking.entities.Currency;
import banking.entities.Receipt;
import banking.errors.InsufficientBalanceError;
import banking.errors.InvalidArgumentsError;
import banking.factories.ReceiptFactory;
import banking.models.OperationResponse;

public class RaiffeisenBank implements Bank {
    public RaiffeisenBank(DataSource dataSource) {
        this.dataSource = dataSource;
        this.exchange = new RaiffeisenExchange();
    }

    public RaiffeisenBank(DataSource dataSource, Exchange exchange) {
        this.dataSource = dataSource;
        this.exchange = exchange;
    }

    final private DataSource dataSource;
    final private Exchange exchange;

    @Override
    public OperationResponse<Client> registerClient(String name) {
        return dataSource.registerClient(name);
    }

    @Override
    public OperationResponse<Account> createAccount(String clientId, Currency currency) {
        return dataSource.createAccount(clientId, currency);
    }

    @Override
    public OperationResponse<Receipt> deposit(String clientId, String accountId, double amount) {
        if(amount <= 0) {
            return new OperationResponse<Receipt>(new InvalidArgumentsError());
        }

        OperationResponse<Account> response = fetchAccountIfOwner(clientId, accountId);
        if(!response.success) {
            return new OperationResponse<Receipt>(new InvalidArgumentsError());
        }

        Account account = response.data;

        updateBalance(account, account.getBalance() + amount);
        return new OperationResponse<Receipt>(true, ReceiptFactory.depositReceipt(account, amount));
    }

    @Override
    public OperationResponse<Receipt> withdraw(String clientId, String accountId, double amount) {
        if(amount <= 0) {
            return new OperationResponse<Receipt>(new InvalidArgumentsError());
        }

        OperationResponse<Account> response = fetchAccountIfOwner(clientId, accountId);
        if(!response.success) {
            return new OperationResponse<Receipt>(new InvalidArgumentsError());
        }

        Account account = response.data;

        if(account.getBalance() < amount) {
            return new OperationResponse<Receipt>(new InsufficientBalanceError());
        }
        updateBalance(account, account.getBalance() - amount);
        return new OperationResponse<Receipt>(true, ReceiptFactory.withdrawReceipt(account, amount));
    }

    @Override
    public OperationResponse<Receipt> transfer(String senderClientId, String senderAccountId, String targetAccountId, double amount) {
        if(amount <= 0) {
            return new OperationResponse<Receipt>(new InvalidArgumentsError());
        }

        OperationResponse<Account> response = fetchAccountIfOwner(senderClientId, senderAccountId);
        if(!response.success) {
            return new OperationResponse<Receipt>(new InvalidArgumentsError());
        }

        Account senderAccount = response.data;
        if(amount > senderAccount.getBalance()) {
            return new OperationResponse<Receipt>(new InsufficientBalanceError());
        }

        OperationResponse<Account> targetResponse = dataSource.fetchAccount(targetAccountId);
        if(!targetResponse.success) {
            return new OperationResponse<Receipt>(new InvalidArgumentsError());
        }

        Account targetAccount = targetResponse.data;
        double amountReceived = exchange.convertToCurrency(amount, senderAccount.getCurrency(), targetAccount.getCurrency());

        updateBalance(senderAccount, senderAccount.getBalance() - amount);
        updateBalance(targetAccount, targetAccount.getBalance() + amountReceived);

        return new OperationResponse<Receipt>(true, ReceiptFactory.transferReceipt(senderAccount, targetAccountId, amount));
    }

    private OperationResponse<Account> fetchAccountIfOwner(String clientId, String accountId) {
        OperationResponse<Client> clientResponse = dataSource.fetchClient(clientId);
        if(!clientResponse.success) {
            return new OperationResponse<Account>(new InvalidArgumentsError());
        }

        OperationResponse<Account> accountResponse = dataSource.fetchAccount(accountId);
        if(!accountResponse.success) {
            return new OperationResponse<Account>(new InvalidArgumentsError());
        }

        Client client = clientResponse.data;
        Account account = accountResponse.data;

        if(account.getOwnerId() != client.getUniqueId()) {
            return new OperationResponse<Account>(false, null);
        }

        return new OperationResponse<Account>(true, account);
    }

    private void updateBalance(Account account, double newBalance) {
        dataSource.updateAccount( new Account(
                account.getUniqueId(),
                account.getOwnerId(),
                account.getCurrency(),
                newBalance
                )
        );
    }
}