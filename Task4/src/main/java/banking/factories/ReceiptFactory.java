package banking.factories;

import banking.entities.Account;
import banking.entities.Receipt;

public class ReceiptFactory {
    public static Receipt depositReceipt(Account account, double amount) {
        return new Receipt(
            String.format(
                "ClientId: %s\n" +
                "AccountId; %s\n" +
                "Deposited %s %.2f.\n" +
                "New balance: %s %.2f.",
                account.getOwnerId(),
                account.getUniqueId(),
                account.getCurrency().label,
                amount,
                account.getCurrency().label,
                account.getBalance()
            )
        );
    }

    public static Receipt withdrawReceipt(Account account, double amount) {
        return new Receipt(
            String.format(
                "ClientId: %s\n" +
                "AccountId; %s\n" +
                "Withdrew %s %.2f.\n" +
                "New balance: %s %.2f.",
                account.getOwnerId(),
                account.getUniqueId(),
                account.getCurrency().label,
                amount,
                account.getCurrency().label,
                account.getBalance()
            )
        );
    }

    public static Receipt transferReceipt(Account account, String targetId, double amount) {
        return new Receipt(
            String.format(
                "ClientId: %s\n" +
                "AccountId; %s\n" +
                "Transferred %s %.2f.\n" +
                "New balance: %s %.2f.",
                account.getOwnerId(),
                account.getUniqueId(),
                account.getCurrency().label,
                amount,
                account.getCurrency().label,
                account.getBalance()
            )
        );
    }
}
