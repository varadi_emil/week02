package banking.contracts;

import banking.entities.Account;
import banking.entities.Client;
import banking.entities.Currency;
import banking.models.OperationResponse;

public interface DataSource {
    public OperationResponse<Client> fetchClient(String clientId);
    public OperationResponse<Account> fetchAccount(String accountId);
    public OperationResponse<Client> registerClient(String name);
    public OperationResponse<Account> createAccount(String clientId, Currency currency);
    public OperationResponse<Account> updateAccount(Account account);
}
