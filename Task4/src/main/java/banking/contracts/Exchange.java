package banking.contracts;

import banking.entities.Currency;

import java.util.HashMap;

public interface Exchange {
    public HashMap<Currency, Double> getRates();
    public double convertToCurrency(double amount, Currency c1, Currency c2);
}
