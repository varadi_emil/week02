package banking.contracts;

public interface UseCase<Type, Params> {
    Type call(Params params);
}
