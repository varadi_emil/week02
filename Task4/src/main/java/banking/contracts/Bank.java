package banking.contracts;

import banking.entities.Account;
import banking.entities.Client;
import banking.entities.Currency;
import banking.entities.Receipt;
import banking.models.OperationResponse;

public interface Bank {
    public OperationResponse<Client> registerClient(String name);
    public OperationResponse<Account> createAccount(String clientId, Currency currency);
    public OperationResponse<Receipt> deposit(String clientId, String accountId, double amount);
    public OperationResponse<Receipt> withdraw(String clientId, String accountId, double amount);
    public OperationResponse<Receipt> transfer(String senderClientId, String senderAccountId, String targetAccountId, double amount);
}
