package banking.datasources;

import banking.contracts.DataSource;
import banking.entities.Account;
import banking.entities.Client;
import banking.entities.Currency;
import banking.errors.InvalidArgumentsError;
import banking.models.OperationResponse;
import com.google.common.hash.Hashing;

import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

public class TotallyReliableDataSource implements DataSource {
    public TotallyReliableDataSource() {
        clients = new HashMap<String, Client>();
        accounts = new HashMap<String, Account>();

        rng = new Random();
    }

    final private HashMap<String, Client> clients;
    final private HashMap<String, Account> accounts;
    final private Random rng;

    @Override
    public OperationResponse<Client> fetchClient(String clientId) {
        Client client = clients.get(clientId);

        if(client == null) {
            return new OperationResponse<>(false, null);
        }

        return new OperationResponse<>(true, client);
    }

    @Override
    public OperationResponse<Account> fetchAccount(String accountId) {
        Account account = accounts.get(accountId);

        if(account == null) {
            return new OperationResponse<>(false, null);
        }

        return new OperationResponse<>(true, account);
    }

    @Override
    public OperationResponse<Client> registerClient(String name) {
        if(name == null) {
            return new OperationResponse<>(false, null);
        }

        if(name.isBlank() || name.isEmpty()) {
            return new OperationResponse<>(false, null);
        }

        Client client = initClient(name);

        return new OperationResponse<>(true, client);
    }

    @Override
    public OperationResponse<Account> createAccount(String clientId, Currency currency) {
        if(clientId == null || currency == null) {
            return new OperationResponse<>(false, null);
        }

        if(clientId.isEmpty() || clientId.isBlank()) {
            return new OperationResponse<>(false, null);
        }

        Client client = clients.get(clientId);
        if(client == null) {
            return new OperationResponse<>(false, null);
        }

        Account account = initAccount(clientId, currency);

        return new OperationResponse<>(true, account);
    }

    @Override
    public OperationResponse<Account> updateAccount(Account account) {
        Account existingAccount = accounts.get(account.getUniqueId());

        if(existingAccount == null) {
            return new OperationResponse<>(new InvalidArgumentsError());
        }

        existingAccount.setBalance(account.getBalance());
        return new OperationResponse<>(true, existingAccount);
    }

    private Client initClient(String name) {
        Integer salt = rng.nextInt(1000000);

        String uniqueId = Hashing.sha256().hashString(name + salt.toString()).toString().toUpperCase();
        while(clients.get(uniqueId) != null) {
            salt = rng.nextInt(1000000);
            uniqueId = Hashing.sha256().hashString(name + salt.toString()).toString().toUpperCase();
        }
        Client client = new Client(
                uniqueId,
                name
        );

        clients.put(uniqueId, client);
        return client;
    }

    private Account initAccount(String clientId, Currency currency) {
        Integer salt = rng.nextInt(1000000);

        String uniqueId = Hashing.sha256().hashString(clientId + currency.label + salt.toString()).toString().toUpperCase();
        while(accounts.get(uniqueId) != null) {
            salt = rng.nextInt(1000000);
            uniqueId = Hashing.sha256().hashString(clientId + salt.toString()).toString().toUpperCase();
        }
        Account account = new Account(uniqueId, clientId, currency);

        accounts.put(uniqueId, account);
        return account;
    }
}
