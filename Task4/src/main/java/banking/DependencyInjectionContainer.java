package banking;

import banking.bank.RaiffeisenBank;
import banking.bank.RaiffeisenExchange;
import banking.contracts.Bank;
import banking.contracts.DataSource;
import banking.contracts.Exchange;
import banking.datasources.TotallyReliableDataSource;
import banking.usecases.*;

import java.util.HashMap;

public class DependencyInjectionContainer {
    private DependencyInjectionContainer() {

    }

    private  HashMap<Class<?>, _Provider> _providers;

    static final private DependencyInjectionContainer _instance = new DependencyInjectionContainer();
    static public DependencyInjectionContainer getInstance() {
        return _instance;
    }

    public void initBanking() {
        _providers = new HashMap<>();

        registerSingleton(DataSource.class, new TotallyReliableDataSource());
        registerSingleton(Exchange.class, new RaiffeisenExchange());
        registerSingleton(Bank.class, new RaiffeisenBank(
                get(DataSource.class),
                get(Exchange.class)
        ));

        registerSingleton(RegisterClient.class, new RegisterClient(
                get(Bank.class)
        ));
        registerSingleton(CreateAccount.class, new CreateAccount(
                get(Bank.class)
        ));
        registerSingleton(Deposit.class, new Deposit(
                get(Bank.class)
        ));
        registerSingleton(Withdraw.class, new Withdraw(
                get(Bank.class)
        ));
        registerSingleton(Transfer.class, new Transfer(
                get(Bank.class)
        ));
    }

    public <T> T get(Class<T> type) {
        return type.cast(_providers.get(type).provide());
    }

    private <T> void registerSingleton(Class<T> type, T instance) {
       _providers.put(type, new _SingletonProvider<T>(instance));
    }
}

interface  _Provider<T> {
    T provide();
}

class _SingletonProvider<T> implements _Provider<T> {
    _SingletonProvider(T instance) {
        _instance = instance;
    }

    final private T _instance;

    @Override
    public T provide() {
        return _instance;
    }
}