package banking.usecases;

import banking.contracts.Bank;
import banking.contracts.UseCase;
import banking.entities.Receipt;
import banking.models.OperationResponse;
import banking.usecases.params.WithdrawParams;

public class Withdraw implements UseCase<OperationResponse<Receipt>, WithdrawParams> {
    public Withdraw(Bank bank) {
        this.bank = bank;
    }

    final private Bank bank;

    @Override
    public OperationResponse<Receipt> call(WithdrawParams params) {
        return bank.withdraw(params.clientId, params.accountId, params.amount);
    }
}
