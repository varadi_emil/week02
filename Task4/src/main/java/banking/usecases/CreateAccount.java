package banking.usecases;

import banking.contracts.Bank;
import banking.contracts.UseCase;
import banking.entities.Account;
import banking.entities.Currency;
import banking.models.OperationResponse;
import banking.usecases.params.CreateAccountParams;

public class CreateAccount implements UseCase<OperationResponse<Account>, CreateAccountParams> {
    public CreateAccount(Bank bank) {
        this.bank = bank;
    }

    final private Bank bank;

    @Override
    public OperationResponse<Account> call(CreateAccountParams params) {
        return bank.createAccount(params.clientId, params.currency);
    }
}

