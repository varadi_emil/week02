package banking.usecases;

import banking.contracts.Bank;
import banking.contracts.UseCase;
import banking.entities.Receipt;
import banking.models.OperationResponse;
import banking.usecases.params.DepositParams;

public class Deposit implements UseCase<OperationResponse<Receipt>, DepositParams> {
    public Deposit(Bank bank) {
        this.bank = bank;
    }

    final private Bank bank;

    @Override
    public OperationResponse<Receipt> call(DepositParams params) {
        return bank.deposit(params.clientId, params.accountId, params.amount);
    }
}
