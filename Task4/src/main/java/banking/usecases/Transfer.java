package banking.usecases;

import banking.contracts.Bank;
import banking.contracts.UseCase;
import banking.entities.Receipt;
import banking.models.OperationResponse;
import banking.usecases.params.TransferParams;

public class Transfer implements UseCase<OperationResponse<Receipt>, TransferParams> {
    public Transfer(Bank bank) {
        this.bank = bank;
    }

    final private Bank bank;

    @Override
    public OperationResponse<Receipt> call(TransferParams params) {
        return bank.transfer(params.senderClientId, params.senderAccountId, params.targetAccountId, params.amount);
    }
}
