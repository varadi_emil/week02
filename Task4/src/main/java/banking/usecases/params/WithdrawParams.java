package banking.usecases.params;

public class WithdrawParams {
    public WithdrawParams(String clientId, String accountId, double amount) {
        this.clientId = clientId;
        this.accountId = accountId;
        this.amount = amount;
    }

    public String clientId;
    public String accountId;
    public double amount;
}
