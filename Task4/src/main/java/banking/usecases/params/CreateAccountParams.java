package banking.usecases.params;

import banking.entities.Currency;

public class CreateAccountParams {
    public CreateAccountParams(String clientId, Currency currency) {
        this.clientId = clientId;
        this.currency = currency;
    }

    public String clientId;
    public Currency currency;
}