package banking.usecases.params;

public class TransferParams {
    public TransferParams(String senderClientId, String senderAccountId, String targetAccountId, double amount) {
        this.senderClientId = senderClientId;
        this.senderAccountId = senderAccountId;
        this.targetAccountId = targetAccountId;
        this.amount = amount;
    }

    public String senderClientId;
    public String senderAccountId;
    public String targetAccountId;
    public double amount;
}
