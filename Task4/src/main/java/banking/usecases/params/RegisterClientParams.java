package banking.usecases.params;

public class RegisterClientParams {
    public RegisterClientParams(String name) {
        this.name = name;
    }

    public String name;
}
