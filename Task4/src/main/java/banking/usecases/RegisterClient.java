package banking.usecases;

import banking.contracts.Bank;
import banking.contracts.UseCase;
import banking.entities.Client;
import banking.models.OperationResponse;
import banking.usecases.params.RegisterClientParams;

public class RegisterClient implements UseCase<OperationResponse<Client>, RegisterClientParams> {
    public RegisterClient(Bank bank) {
        this.bank = bank;
    }

    final private Bank bank;

    @Override
    public OperationResponse<Client> call(RegisterClientParams params) {
        return bank.registerClient(params.name);
    }
}
