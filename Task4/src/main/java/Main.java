import banking.DependencyInjectionContainer;
import banking.entities.Account;
import banking.entities.Client;
import banking.entities.Currency;
import banking.entities.Receipt;
import banking.errors.InsufficientBalanceError;
import banking.errors.InvalidArgumentsError;
import banking.errors.NoError;
import banking.models.OperationResponse;
import banking.usecases.*;
import banking.usecases.params.*;

import java.io.FileWriter;
import java.io.IOException;

public class Main {

    private static final DependencyInjectionContainer di = DependencyInjectionContainer.getInstance();
    private static RegisterClient registerClient;
    private static CreateAccount createAccount;
    private static Deposit deposit;
    private static Withdraw withdraw;
    private static Transfer transfer;

    public static void main(String[] args) {
        di.initBanking();
        initUseCases();

        registerTwoClientsWithTwoAccountsAndTransferSomeMoney();
    }

    public static void initUseCases() {
        registerClient = di.get(RegisterClient.class);
        createAccount = di.get(CreateAccount.class);
        deposit = di.get(Deposit.class);
        withdraw = di.get(Withdraw.class);
        transfer = di.get(Transfer.class);
    }

    public static void registerTwoClientsWithTwoAccountsAndTransferSomeMoney() {
        FileWriter myWriter;
        try {
            myWriter = new FileWriter("logs.txt");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            return;
        }

        RegisterClientParams params1 = new RegisterClientParams("Xi Jinping");
        RegisterClientParams params2 = new RegisterClientParams("Emil Boc");
        OperationResponse<Client> response1 = registerClient.call(params1);
        OperationResponse<Client> response2 = registerClient.call(params2);

        if(!response1.success) {
            printError(response1);
            return;
        }

        if(!response2.success) {
            printError(response2);
            return;
        }

        Client client1 = response1.data;
        Client client2 = response2.data;

        System.out.println(client1);
        System.out.println(client2);
        writeToFile(myWriter, client1.toString());
        writeToFile(myWriter, client2.toString());

        CreateAccountParams accountParams1 = new CreateAccountParams(client1.getUniqueId(), Currency.EUR);
        CreateAccountParams accountParams2 = new CreateAccountParams(client2.getUniqueId(), Currency.RON);

        OperationResponse<Account> accountResponse1 = createAccount.call(accountParams1);
        OperationResponse<Account> accountResponse2 = createAccount.call(accountParams2);

        if(!accountResponse1.success) {
            printError(accountResponse1);
            return;
        }

        if(!accountResponse2.success) {
            printError(accountResponse2);
            return;
        }

        Account account1 = accountResponse1.data;
        Account account2 = accountResponse2.data;

        System.out.println(account1);
        System.out.println(account2);
        writeToFile(myWriter, account1.toString());
        writeToFile(myWriter, account2.toString());

        DepositParams depositParams1 = new DepositParams(client1.getUniqueId(), account1.getUniqueId(), 1000000);
        DepositParams depositParams2 = new DepositParams(client2.getUniqueId(), account2.getUniqueId(), 10);

        OperationResponse<Receipt> depositResponse1 = deposit.call(depositParams1);
        OperationResponse<Receipt> depositResponse2 = deposit.call(depositParams2);

        if(!depositResponse1.success) {
            printError(depositResponse1);
            return;
        }

        if(!depositResponse2.success) {
            printError(depositResponse2);
            return;
        }

        System.out.println(depositResponse1.data);
        System.out.println(depositResponse2.data);
        System.out.println(account1);
        System.out.println(account2);
        writeToFile(myWriter, depositResponse1.data.toString());
        writeToFile(myWriter, depositResponse2.data.toString());
        writeToFile(myWriter, account1.toString());
        writeToFile(myWriter, account2.toString());

        WithdrawParams withdrawParams1 = new WithdrawParams(client1.getUniqueId(), account1.getUniqueId(), 100000);
        WithdrawParams withdrawParams2 = new WithdrawParams(client2.getUniqueId(), account2.getUniqueId(), 5);

        OperationResponse<Receipt> withdrawResponse1 = withdraw.call(withdrawParams1);
        OperationResponse<Receipt> withdrawResponse2 = withdraw.call(withdrawParams2);

        if(!withdrawResponse1.success) {
            printError(withdrawResponse1);
            return;
        }

        if(!withdrawResponse2.success) {
            printError(withdrawResponse2);
            return;
        }

        System.out.println(withdrawResponse1.data);
        System.out.println(withdrawResponse2.data);
        System.out.println(account1);
        System.out.println(account2);
        writeToFile(myWriter, withdrawResponse1.data.toString());
        writeToFile(myWriter, withdrawResponse2.data.toString());
        writeToFile(myWriter, account1.toString());
        writeToFile(myWriter, account2.toString());

        TransferParams transferParams1 = new TransferParams(client1.getUniqueId(), account1.getUniqueId(), account2.getUniqueId(), 200000);
        TransferParams transferParams2 = new TransferParams(client2.getUniqueId(), account2.getUniqueId(), account1.getUniqueId(), 5);

        OperationResponse<Receipt> transferResponse1 = transfer.call(transferParams1);
        OperationResponse<Receipt> transferResponse2 = transfer.call(transferParams2);

        if(!transferResponse1.success) {
            printError(transferResponse1);
        }

        if(!transferResponse2.success) {
            printError(transferResponse2);
        }

        System.out.println(transferResponse1.data);
        System.out.println(transferResponse2.data);
        System.out.println(account1);
        System.out.println(account2);
        writeToFile(myWriter, transferResponse1.data.toString());
        writeToFile(myWriter, transferResponse2.data.toString());
        writeToFile(myWriter, account1.toString());
        writeToFile(myWriter, account2.toString());

        try {
            myWriter.close();
        }
        catch (IOException e) {
            System.out.println("Can't close file ?!");
        }
    }

    public static void writeToFile(FileWriter writer, String string) {
        try {
           writer.write(string);
        }
        catch (IOException e) {
            System.out.println("Logging gone wrong");
        }
    }

    public static <T> void printError(OperationResponse<T> response) {
        if(response.error instanceof NoError) {
            return;
        }

        if(response.error instanceof InvalidArgumentsError) {
            System.out.println("Invalid arguments mate!");
            return;
        }

        if(response.error instanceof InsufficientBalanceError) {
            System.out.println("Come back when you are a bit mmmmmm... richer!");
        }
    }
}
